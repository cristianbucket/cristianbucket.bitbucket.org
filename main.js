(function(){
		var detalle=document.querySelector('#detalle');
		var listado=document.querySelector('#listado');
		detalle.show=listado.show=function(){
			this.className=this.className.replace(' hidden','');
		};
		detalle.hide=listado.hide=function () {
			this.show();
			this.className+=' hidden';
		};
    var vaciar = function(elemento){
        while(elemento.childNodes.length>0)
            elemento.removeChild(elemento.childNodes[0]);
    };
    var checkStatus = function(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    };
    var parseJSON = function (response) {
        return response.json()
    };
    var crearItem = function(primary,textBody){
        var li=document.createElement('li');
        li.className='mdl-list__item mdl-list__item--three-line';
        var span=document.createElement('span');
        span.className='mdl-list__item-primary-content';
        var span2=document.createElement('span');
        span2.innerHTML=primary;
        span.appendChild(span2);
        var span2=document.createElement('span');
        span2.className='mdl-list__item-text-body';
        span2.innerHTML=textBody;
        span.appendChild(span2);
        li.appendChild(span);
        return li;
    };
    var mostrarDetalle = function(response){
        vaciar(detalle);
        var item=crearItem('Volver','');
        item.addEventListener('click',function(){
        	detalle.hide();
        	listado.className=listado.className.replace(' hidden','');
				});
				detalle.appendChild(item);
        for(var i in response){
            if(typeof response[i] == 'object' && response[i] != null)
                response[i]=response[i].name!=undefined?response[i].name:JSON.stringify(response[i]);
            var item=crearItem(i,response[i]);
            detalle.appendChild(item);
        }
				listado.hide();
				detalle.show();
    };
    var mostrarDetalle2 = function(response){
    		for(var i in response){
            if(typeof response[i] == 'object' && response[i] != null)
                response[i]=response[i].name!=undefined?response[i].name:JSON.stringify(response[i]);
            var item=crearItem(i,response[i]);
            detalle.appendChild(item);
        }
    };
    var urlApi={};
    urlApi.cargar=function(data){
			for(var i in data)
				this[i]=data[i];
		};
    fetch('urlApi.json')
			.then(checkStatus)
			.then(parseJSON)
			.then(function(json){
				urlApi.cargar(json);
			})
			.catch(function (error) {
				console.error(error);
			});
    var mostrarMeta = function(){
			fetch(urlApi.distrito[this.distrito]+urlApi.api.metasTodas.replace('*',this.id))
				.then(checkStatus)
				.then(parseJSON)
				.then(mostrarDetalle)
				.catch(function(error){
					console.error(error);
				});
    };
    var mostrarMetas = function(distrito){
    	return function(response) {
				for (var i in response.goals) {
					var item = crearItem(response.goals[i].name, response.goals[i].description);
					item.id = response.goals[i].id;
					item.className += ' mdl-button mdl-js-button mdl-js-ripple-effect '+distrito.replace(/\s/,'-');
					item.addEventListener('click', mostrarMeta);
					listado.appendChild(item);
					componentHandler.upgradeAllRegistered();
					item.distrito = distrito;
				}
				detalle.hide();
				listado.show();
			}
    };
    var mostrarProyecto = function(){
        fetch(urlApi.distrito[this.distrito]+urlApi.api.proyectosTodos.replace('*',this.id))
        .then(checkStatus)
        .then(parseJSON)
        .then(mostrarDetalle)
        .catch(function(error){
            console.error(error)
        });
    };
    var miPosicion={};
    var manejarPosicion = function(posicion){
			miPosicion.latitude=posicion.coords.latitude;
			miPosicion.longitude=posicion.coords.longitude;
		};
    if(miPosicion.latitude==undefined){
			if (navigator.geolocation)
				navigator.geolocation.getCurrentPosition(manejarPosicion);
			else
				console.log("Geolocation no soportado.");
		}
    var distancia = function(lat1, lon1, lat2, lon2) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        dist = dist * 1.609344
        return dist
    }
    var mostrarProyectos = function(distrito){
    	return function(response){
				for(var i in response.projects){
					var item=crearItem(response.projects[i].name,response.projects[i].percentage+'% | Lat: '+response.projects[i].latitude+' Lon: '+response.projects[i].longitude+(miPosicion.latitude!=undefined?' | ('+parseInt(100*distancia(miPosicion.latitude,miPosicion.longitude,response.projects[i].latitude,response.projects[i].longitude))/100+' Kms.)':''));
					item.id=response.projects[i].id;
					item.distancia=distancia(miPosicion.latitude,miPosicion.longitude,response.projects[i].latitude,response.projects[i].longitude);
					item.className+=' mdl-button mdl-js-button mdl-js-ripple-effect '+distrito.replace(/\s/,'-');
					item.addEventListener('click',mostrarProyecto);
					listado.appendChild(item);
					componentHandler.upgradeAllRegistered();
					item.distrito=distrito;
				}
				var ordenar=function(a,b){
					if(a.distancia<b.distancia)
						return -1;
					else if(b.distancia<a.distancia)
						return 1;
					else
						return 0;
				};
				var items=[];
				for(var i=0;i<listado.childNodes.length;i++)
					items.push(listado.childNodes[i]);
				items.sort(ordenar);
				for(var i=0;i<items.length;i++)
					listado.appendChild(items[i]);
				detalle.hide();
				listado.show();
			};
		};
    var mostrarObjetivos = function(distrito) {
			return function (response) {
				for (var i in response.objectives) {
					var item = crearItem(response.objectives[i].name, response.objectives[i].url);
					item.id = response.objectives[i].id;
					item.className += ' mdl-button mdl-js-button mdl-js-ripple-effect ' + distrito.replace(/\s/, '-');
					listado.appendChild(item);
					item.distrito = distrito;
				}
				detalle.hide();
				listado.show();
			};
		};
    var mostrarEmpresa = function(){
        fetch(urlApi.distrito[this.distrito]+urlApi.api.empresasMetas.replace('*',this.id))
        .then(checkStatus)
        .then(parseJSON)
        .then(mostrarDetalle)
        .catch(function(error){
            console.error(error)
        });
        fetch(urlApi.distrito[this.distrito]+urlApi.api.empresasObjetivos.replace('*',this.id))
        .then(checkStatus)
        .then(parseJSON)
        .then(mostrarDetalle2)
        .catch(function(error){
            console.error(error)
        });
    };
    var mostrarEmpresas = function(distrito){
    	return function(response){
				for(var i in response.companies){
					var item=crearItem(response.companies[i].name,response.companies[i].name_url);
					item.id=response.companies[i].id;
					item.className+=' mdl-button mdl-js-button mdl-js-ripple-effect '+distrito.replace(/\s/,'-');
					item.addEventListener('click',mostrarEmpresa);
					listado.appendChild(item);
					componentHandler.upgradeAllRegistered();
					item.distrito=distrito;
				}
				detalle.hide();
				listado.show();
			};
		};
    var consultarAPI=function(distrito){
    	console.log('Consultando: '+distrito);
    	return function(recurso) {
				fetch(urlApi.distrito[distrito] + recurso)
					.then(checkStatus)
					.then(parseJSON)
					.then(recurso === 'goals' ? mostrarMetas(distrito) : recurso === 'projects' ? mostrarProyectos(distrito) : recurso === 'objectives' ? mostrarObjetivos(distrito) : mostrarEmpresas(distrito))
					.catch(function (error) {
						console.error(error)
					});
			}
    };


    var distritos=['Godoy Cruz','Maipu','Las Heras','Ciudad','Lujan','Guaymallen'];
    var consultarAPIS=function(){
			vaciar(detalle);
			vaciar(listado);
    	for(var i=0;i<distritos.length;i++)
    		consultarAPI(distritos[i])(this.id);
		};
    var recursos=document.querySelectorAll('.recursos');
    for(var i=0,n=recursos.length;i<n;i++) {
			recursos[i].addEventListener('click', consultarAPIS);
			recursos[i].addEventListener('click',function(){
				var obfuscator=document.querySelector('.mdl-layout__obfuscator');
				if(obfuscator.classList?obfuscator.classList.contains('is-visible'):!!obfuscator.className.match(new RegExp("\\bis-visible\\b")))
					obfuscator.click();
			});
		}

})();